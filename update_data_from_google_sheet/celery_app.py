import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'update_data_from_google_sheet.settings')

app = Celery('shop_stat', broker='redis://localhost:6379/0')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'update_data-every-30-minute': {
        'task': 'shopStat.tasks.update_data',
        'schedule': crontab("*/2 * * * *"),
    },
}