from time import strptime

import gspread
import requests
import datetime
import xml.etree.ElementTree as ET

from shopStat.models import Shop_stat
from update_data_from_google_sheet.celery_app import app


@app.task
def update_data():
    doc_data = read_data()
    current_curracy = get_curracy()
    Shop_stat.objects.all().delete()
    for data_list in doc_data:
        number = int(data_list[0])
        order_number = str(data_list[1])
        cost_dollar = float(data_list[2])
        delivery_date = datetime.datetime.strptime(data_list[3], "%d.%m.%Y").date()
        cost_rub = float(data_list[2])*current_curracy
        Shop_stat.objects.create(number=number,
                  order_number=order_number,
                  cost_dollar=cost_dollar,
                  delivery_date=delivery_date,
                  cost_rubbles=cost_rub)

def read_data():
    gc = gspread.service_account(filename='google_sheet.json')

    sh = gc.open("test")
    worksheet = sh.sheet1
    return worksheet.get_all_values()[1:]

def get_curracy() -> float:
    BASE_URL = "https://www.cbr.ru/scripts/XML_daily.asp"
    resp = requests.post(BASE_URL)
    root = ET.fromstring(resp.content)
    USD_elem = root.findall(".//*[@ID='R01235']")
    return float(USD_elem[0].find("Value").text.replace(",", "."))