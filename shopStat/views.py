from datetime import datetime

from django.shortcuts import render
from django.http import HttpResponse

from rest_framework import generics
from shopStat.models import Shop_stat
from .serializers import ShopStatSerializer

from shopStat.tasks import update_data
# Create your views here.
def index(request):
    #update_data()
    return HttpResponse("Страница приложения shopStat.")

class ShopStatView(generics.ListCreateAPIView):
    queryset = Shop_stat.objects.all()
    serializer_class = ShopStatSerializer


