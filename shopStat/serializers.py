from rest_framework import serializers

from shopStat.models import Shop_stat


class ShopStatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop_stat
        fields = ('id', 'number', 'order_number',
                  'cost_dollar', 'cost_rubbles', 'delivery_date')
    