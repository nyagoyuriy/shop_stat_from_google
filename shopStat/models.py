from django.db import models
from django.db import models

# Create your models here.

class Shop_stat(models.Model):
    number = models.IntegerField()
    order_number = models.CharField(max_length=255)
    cost_dollar = models.FloatField()
    cost_rubbles = models.FloatField()
    delivery_date = models.DateField()