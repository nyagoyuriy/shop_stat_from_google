from django.urls import path

from .views import ShopStatView

urlpatterns = [
    path('Shop_stat/', ShopStatView.as_view()),
]