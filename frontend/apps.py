from django.apps import AppConfig


class Frontend17Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'frontend'
