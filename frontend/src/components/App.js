import React, { Component } from "react";
import { render } from "react-dom";
import {BootstrapTable, TableHeaderColumn} from 
       'react-bootstrap-table'


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded : false,
      placeholder: "Loading"
    }
  }

  componentDidMount() {
    fetch("api/Shop_stat/")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return {placeholder: "Something went wrong"};
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    return (
//        {this.state.data.map(contact => {
//          return (

            <BootstrapTable id="data_table" data={this.state.data}>
            <TableHeaderColumn isKey
                             dataField='number'
                             dataAlign='center'
                             headerAlign="left"
                             width="30"
                             border="5px"
                             tdStyle={
                                 {backgroundColor: 'green'}}>
            №
          </TableHeaderColumn>
          <TableHeaderColumn dataField='order_number'
                             dataAlign='center'
                             headerAlign="center"
                             width="20%"
                             tdStyle={
                              {backgroundColor: 'green'}}>
            order
          </TableHeaderColumn>
          <TableHeaderColumn dataField='cost_dollar'
                             dataAlign='center'
                             headerAlign="center"
                             width="80"
                             tdStyle={
                              {backgroundColor: 'green'}}>
            cost $
          </TableHeaderColumn>
          <TableHeaderColumn dataField='cost_rubbles'
                             dataAlign='center'
                             headerAlign="center"
                             tdStyle={
                              {backgroundColor: 'green'}}>
            cost rub
          </TableHeaderColumn>
          <TableHeaderColumn dataField='delivery_date'
                             dataAlign='center'
                             headerAlign="center"
                             width="80"
                             tdStyle={
                              {backgroundColor: 'green'}}>
            delivery date
          </TableHeaderColumn>
            </BootstrapTable>



//            <li key={contact.id}>
//              {contact.order_number} - {contact.cost_dollar}
//            </li>
          );
//        })}
 //   );
 // }
  }
}



const appDiv = document.getElementById("app");
render(<App />, appDiv);